import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddMotorcyclePageRoutingModule } from './add-motorcycle-routing.module';

import { AddMotorcyclePage } from './add-motorcycle.page';
import { ComponentsModule } from 'src/app/components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AddMotorcyclePageRoutingModule,
    ComponentsModule
  ],
  declarations: [AddMotorcyclePage]
})
export class AddMotorcyclePageModule { }
