import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-add-motorcycle',
  templateUrl: './add-motorcycle.page.html',
  styleUrls: ['./add-motorcycle.page.scss'],
})
export class AddMotorcyclePage implements OnInit {
  model: string = '';
  brand: string = '';
  price: number = 0;
  year: string = '';
  photo: string = '';

  cameraImage: string | ArrayBuffer = '../../../assets/camera.svg';

  constructor(private data: DataService, private router: Router) {}

  ngOnInit() {}

  cameraImageChange(file: File) {
    this.photo = file[0];
    const reader = new FileReader();
    reader.readAsDataURL(file[0]);
    reader.onload = () => {
      this.cameraImage = reader.result;
    };
  }

  addNewMotorcycle() {
    const formData = new FormData();

    const priceAsString = this.price + '';

    formData.append('foto', this.photo);
    formData.append('marca', this.brand);
    formData.append('modelo', this.model);
    formData.append('year', this.year);
    formData.append('precio', priceAsString);

    //     const form = new FormData();
    // form.append("foto", "C:\\Users\\rafa\\Pictures\\emojis\\emoji-extractor-master\\images\\160x160\\32.png");
    // form.append("marca", "Ducati");
    // form.append("modelo", "11111Ducati monster");
    // form.append("year", "2010");
    // form.append("precio", "3330");
    // form.append("", "");

    this.data.addNewMotorcycle(formData);

    this.router.navigate(['/motorcycles']);
  }
}
