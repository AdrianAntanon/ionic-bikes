import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MotorcyclesPageRoutingModule } from './motorcycles-routing.module';

import { MotorcyclesPage } from './motorcycles.page';
import { ComponentsModule } from '../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MotorcyclesPageRoutingModule,
    ComponentsModule
  ],
  declarations: [MotorcyclesPage]
})
export class MotorcyclesPageModule { }
