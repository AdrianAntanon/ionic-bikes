import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MotorcycleDetailPage } from './motorcycle-detail.page';

const routes: Routes = [
  {
    path: '',
    component: MotorcycleDetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MotorcycleDetailPageRoutingModule {}
