import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'motorcycles',
    loadChildren: () => import('./motorcycles/motorcycles.module').then( m => m.MotorcyclesPageModule)
  },
  {
    path: '',
    redirectTo: 'motorcycles',
    pathMatch: 'full'
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
