import { Injectable } from '@angular/core';

import { Motorcycle } from '../motorcycles/motorcycles.model';

@Injectable({
  providedIn: 'root',
})
export class DataService {
  BASE_URL: string = `http://motos.puigverd.org`;
  OWN_API_REST: string =
    'http://adrian-antanyon-7e3.alwaysdata.net/apimotorcycles';
  motorcycles: Motorcycle[] = [];
  singleMotorcycle: Motorcycle;

  constructor() {}

  async getMotorcycles() {
    this.motorcycles = [];
    this.motorcycles = await (
      await fetch(
        /* `${this.BASE_URL}/motos` */ `${this.OWN_API_REST}/getMotos`
      )
    ).json();

    return this.motorcycles;
  }

  setSingleMotorcycle(currentMotorcycle: Motorcycle) {
    this.singleMotorcycle = currentMotorcycle;
  }

  getSingleMotorcycle() {
    return this.singleMotorcycle;
  }

  async getFilteredMotorcycle(brand: string) {
    this.motorcycles = await (
      await fetch(`${this.OWN_API_REST}/getMotos?marca=${brand}`)
    ).json();

    return this.motorcycles;
  }

  async deleteMotorcycle(id: number) {
    const motorcycleDeleted = await fetch(
      /* `${this.BASE_URL}/moto/${id}` */ `${this.OWN_API_REST}/deleteMoto/${id}`,
      {
        method: 'DELETE',
      }
    );
    return motorcycleDeleted;
  }

  async addNewMotorcycle(newMotorcycle) {
    const newBike = await fetch(`${this.BASE_URL}/moto/foto`, {
      method: 'POST',
      body: newMotorcycle,
    });
  }
}
